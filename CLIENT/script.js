// reg/login display form part
const containerHeader = document.querySelector('.container-header');
containerHeader.addEventListener('click', (e) => {
    if(e.target.innerHTML === 'Registration') {
        const loginForm = document.querySelector('.login-form')
        const registrationForm = document.querySelector('.registration-form')
        loginForm.classList.add('display-none')
        registrationForm.classList.remove('display-none');
    }
    if(e.target.innerHTML === 'Login') {
        const loginForm = document.querySelector('.login-form')
        const registrationForm = document.querySelector('.registration-form')
        registrationForm.classList.add('display-none')
        loginForm.classList.remove('display-none');
    }
})

// registration part
const buttonRegister = document.querySelector('.button-register');
buttonRegister.addEventListener('click', () => {
    fetch('http://localhost:8080/api/auth/register', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify({
            name: document.querySelector('.name-registration').value,
            username: document.querySelector('.username-registration').value,
            password: document.querySelector('.pass-registration').value
        })
    })
    .then(response => {
        if(response.ok) {
            response.json().then((data) => {
                document.querySelector('.registration-answer').innerHTML = `${data.message}`;
            })           
        } else {
            response.json().then(() => {
                document.querySelector('.registration-answer').innerHTML = `Registration failed..`;
            })
        }
    })
})

// login part
const buttonLogin = document.querySelector('.button-login');
buttonLogin.addEventListener('click', () => {
    fetch('http://localhost:8080/api/auth/login', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify({
            username: document.querySelector('.username-login').value,
            password: document.querySelector('.username-pass').value
        })
    })
    .then(response => {
        if(response.ok) {
            response.json().then((data) => {
                document.querySelector('.login-answer').innerHTML = `${data.message}`;
                localStorage.setItem('token', `${data.jwt_token}`);
                const showUserInfo = document.querySelector('.container-user-info');
                showUserInfo.classList.remove('display-none');
                const loginForm = document.querySelector('.login-form');
                loginForm.classList.add('display-none');
            })           
        } else {
            response.json().then((data) => {
                document.querySelector('.login-answer').innerHTML = `${data.message}`;
            })
        }
    })
})

// get user profile info
const buttonShowUserInfo = document.querySelector('.button-show-info')
buttonShowUserInfo.addEventListener('click', () => {
    fetch('http://localhost:8080/api/users/me', {
        method: 'GET',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })
    .then(response => {
        if(response.ok) {
            response.json().then((data) => {
                console.log(data.user)
                const newDiv = document.createElement('button')
                document.querySelector('.user-info').innerHTML = 
                `Name - ${data.user.name}<br>
                Username - ${data.user.username}<br>
                User created time - ${data.user.createdDate}`;
                const userDelete = document.querySelector('.button-user-delete')
                userDelete.classList.remove('display-none');
            })           
        } else {
            response.json().then((err) => {
                console.log(err)
            })
        }
    })
})

// delete user part
const buttonUserDelete = document.querySelector('.button-user-delete')
buttonUserDelete.addEventListener('click', () => {
    fetch('http://localhost:8080/api/users/me', {
        method: 'DELETE',
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
            authorization: `Bearer ${localStorage.getItem('token')}`
        },
    })
    .then(response => {
        if(response.ok) {
            document.querySelector('.user-info').innerHTML = 'Account deleted successfully. You can try to login with it again to double check and make sure'       
        } else {
            response.json().then((err) => {
                console.log(err)
            })
        }
    })
})