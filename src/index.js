const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
app.use(cors())
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://bohdankoliada:bk1234@bohdankoliadamongodbclu.mrpf6uw.mongodb.net/notes?retryWrites=true&w=majority');

const { notesRouter } = require('./notesRouter');
const { authRouter } = require('./authRouter');
const { usersRouter } = require('./usersRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);
app.use('/api/users/me', usersRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
