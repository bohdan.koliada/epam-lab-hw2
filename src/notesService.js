const { Note } = require('./models/Notes');

// create(add) Note for user
function createNote(req, res) {
  const { text } = req.body;
  const { userId } = req.user;
  const note = new Note({
    text,
    userId,
  });
  note.save()
    .then(() => { res.status(200).send({ message: 'Success' }); });
}

// get the list of notes for authorized user
const getNotes = (req, res) => Note.find({ userId: req.user.userId }, '-__v')
  .then((result) => {
    res.status(200).json({
      offset: 0,
      limit: 0,
      count: 0,
      notes: result,
    });
  });

// get 1 note info func
const getNote = (req, res) => Note.findById(req.params.id)
  .then((note) => {
    // res.json(note);
    res.status(200).json({
      note: note
    });
  });

// mark note as completed by id of current user
// (Check/uncheck user's note by id, value for completed field should be changed to opposite)?
const markMyNoteCompletedById = (req, res) => Note.findByIdAndUpdate(
  { _id: req.params.id, userId: req.user.userId },
  { $set: { completed: true } },
)
  .then(() => {
    res.json({ message: 'Note was marked completed' });
  });

// deleting user's note
const deleteNote = (req, res) => Note.findByIdAndDelete(req.params.id)
  .then(() => {
    res.json({ message: 'Note was deleted' });
  });

// update note info by id of current user func
const updateMyNoteById = (req, res) => {
  const { text } = req.body;
  return Note.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { text } },
  )
    .then(() => {
      res.json({ message: 'Note was updated' });
    });
};

module.exports = {
  createNote,
  getNotes,
  getNote,
  deleteNote,
  updateMyNoteById,
  markMyNoteCompletedById,
};
