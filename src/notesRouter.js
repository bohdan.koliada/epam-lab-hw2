const express = require('express');

const router = express.Router();
const {
  createNote, getNotes, getNote, deleteNote,
  updateMyNoteById, markMyNoteCompletedById,
} = require('./notesService');

const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getNotes);

router.get('/:id', authMiddleware, getNote);

router.patch('/:id', authMiddleware, markMyNoteCompletedById);

router.put('/:id', authMiddleware, updateMyNoteById);

router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
