const bcrypt = require('bcryptjs');
const { User } = require('./models/Users');

// get users info func
const getUserInfo = (req, res) => User.findById(req.user.userId)
  .then((userInfo) => {
    res.status(200).json({
      user: userInfo,
    });
  });

// changing user password
const changeUserPassword = async (req, res) => {
  const { newPassword } = req.body;
  const user = await User.findById(req.user.userId);
  if (await bcrypt.compare(String(req.body.oldPassword), String(user.password))) {
    return User.findByIdAndUpdate(
      { _id: req.user.userId },
      { $set: { password: await bcrypt.hash(newPassword, 10) } },
    )
      .then(() => {
        res.json({ message: 'User account password was changed' });
      });
  }
  return res.json({ message: 'Incorrect old password' });
};

// deleting user's profile
const deleteUser = (req, res) => User.findByIdAndDelete(req.user.userId)
  .then(() => {
    res.json({ message: 'User account was deleted' });
  });

module.exports = {
  getUserInfo,
  changeUserPassword,
  deleteUser,
};
